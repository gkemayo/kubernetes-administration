cp /home/georges/.ssh/vms_key_rsa ./privkey #change the path to the private ssh key of cluster Machines ('/home/georges/.ssh/vms_key_rsa'), located in the Ansible ctrl Machine, with your own
chmod 0600 ./privkey
ansible-playbook cluster-upgrade-playbook.yml -i ./inventory.ini --private-key=./privkey --ask-become-pass --timeout=3600  #--check
rm ./privkey
